# Jenkins

> you should have docker desktop installed in your host

## Build images

- make build && make up

## download jenkins-cli.jar

- curl 'localhost:8080/jnlpJars/jenkins-cli.jar' > jenkins-cli.jar

## generate personal token

- user profile -> configure -> API token -> Add new Token

## jenkins-cli

- java -jar jenkins-cli.jar -s http://localhost:8080/ -auth USER:API_TOKEN help
- java -jar jenkins-cli.jar -s http://localhost:8080/ -auth USER:API_TOKEN groovy = < tools/plugins.groovy | vim -

## agent in cloud docker

- setup Manage Jenkins -> Configure Clouds -> setup
    1. host uri: tcp://docker/2376
    2. server credentials
        - set cat client/key.pem
        - set cat client/cert.pem
        - set cat client/ca.pem
    3. try test connection
